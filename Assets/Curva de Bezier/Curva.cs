﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curva : MonoBehaviour{

	#region Variables
	public GameObject target;
	public Transform p0;
	public Transform p1;
	public Transform p2;
	public Transform p3;
	public float t = 0.0f;
	public int reductor = 2;
	#endregion

	#region UnityMethods
    void Update(){
		/* De 3 puntos
		 * target.transform.position = Vector3.Lerp(
			Vector3.Lerp(p0.position, p1.position, t),
			Vector3.Lerp(p1.position, p2.position, t),
			t);
		t += Time.deltaTime/reductor;*/

		target.transform.position = Vector3.Lerp(
			Vector3.Lerp(Vector3.Lerp(p0.position, p1.position, t), Vector3.Lerp(p1.position, p2.position, t), t),
			Vector3.Lerp(Vector3.Lerp(p1.position, p2.position, t), Vector3.Lerp(p2.position, p3.position, t), t),
			t);
		t += Time.deltaTime / reductor;
    }
    #endregion
}
