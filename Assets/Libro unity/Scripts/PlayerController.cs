﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float velX = 20f;
    public float velZ = 30f;
    public Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        /*if (Input.GetKey(KeyCode.W)) {
            transform.Translate((Vector3.right * -velX)*Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S)) {
            transform.Translate((Vector3.right * velX) * Time.deltaTime);
        }*/
        if (Input.GetKey(KeyCode.W)) {
            rb.velocity -= transform.right * (velX * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S)) {
            rb.velocity += transform.right * (velX * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A)) {
            transform.Rotate((Vector3.up * -velZ) * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D)) {
            transform.Rotate((Vector3.up * velZ) * Time.deltaTime);
        }
    }
}
