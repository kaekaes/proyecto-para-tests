﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lootbox : MonoBehaviour
{
    public int quantDeDrop;
    public GameObject canvas;

    [System.Serializable]
    public class lootcard {
        public string NombreCarta;
        public GameObject cartaGO;
        public int porcentaje;
    }

    public List<lootcard> Drop = new List<lootcard>();

    public void LootAction() {
        int drops = quantDeDrop;
        while (drops > 0) {
            foreach (var carta in Drop) {
                int chance = Random.Range(0, 100);
                if (drops > 0) {
                    if (chance < carta.porcentaje) {
                        Instantiate(carta.cartaGO, canvas.transform);
                        Debug.Log("Dropeada: " + carta.NombreCarta);
                        drops--;
                    } else {
                        Debug.Log("No dropeada la carta: " + carta.NombreCarta);
                    }
                }
            }
        }
    }
}
