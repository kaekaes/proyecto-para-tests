﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class SO_Card : ScriptableObject {

    public string cardname;
    public int price;
    public int dmg;
    public int range;
    public int att_speed;
    public string special;
    public Color special_color;

    public int card_commonness;
    public int card_rarity;

    public Sprite card_bg;
    public Sprite Icon;
}
