﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_homing : MonoBehaviour {

    public Transform target;
    public float speed = 5f;
    public float distanceThisFrame;
    public int damage = 4;

    public bool canGoToEnemy = true;
    public float goToEnemyTime = 1f;
    public float rotSpeed = 50f;

    public Rigidbody2D rb;

    public bool left = false;
	public Vector3 dir;
    //Seek es una funcion llamada desde la torre que traspasa el objetivo y lo guarda.
    public void Seek(Transform _target) {
        target = _target;
    }

    //Bill tiene 2 estados, recien lanzado y teledirigido. Al inicio hago que se inicie la coroutine del recien lanzado durante goToEnemyTime
    void Start() {
        StartCoroutine(Count());
		 if (target.position.x < transform.position.x) {
			 left = true;
		 }
    }

    void Update () {

        //Si no tiene objetivo desaparece
        if (target == null) {
            Destroy(gameObject);
            return;
        }

        //mientras se ejecuta la coroutine la bala va hacia un lado, el lado es un calculo de posiciones
        if (canGoToEnemy) {
            //Cambiar todo esto

            if (!left) {
                dir = Vector3.right;
                transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, 0, 0);
            } else {
                dir = Vector3.left;
                transform.rotation = new Quaternion(transform.rotation.x, 0, 180, 0);
				GetComponent<SpriteRenderer>().flipY = true;
            }
            //avanza hacia delante
            distanceThisFrame = speed * Time.deltaTime;
            transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        }
    }

    private void FixedUpdate() {
        if (!canGoToEnemy) {
			dir = (Vector2)target.position - rb.position;
            dir.Normalize();

            float rotate = Vector3.Cross(dir, transform.right).z;

            rb.angularVelocity = -rotate * rotSpeed;
            rb.velocity = transform.right * speed;
        }
    }

    void HitTarget() {
        //Destroy(gameObject);

        //por ahora destruye al enemigo directamente
        //Destroy(target.gameObject);

        //EJEMPLO
        //PlayerStats.Money += 30;
    }

    //tiempo para que despues de salir del cañon vaya al enemigo
    IEnumerator Count() {
        yield return new WaitForSeconds(goToEnemyTime);
        canGoToEnemy = false;
    }
}
