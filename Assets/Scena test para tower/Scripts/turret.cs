﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turret : MonoBehaviour {

    private Transform target;
    private Enemy targetEnemy;
    public string enemyTag = "enemy";

    public float range = 6f;
    public GameObject bulletPref;
    public float fireRate = 1f;
    public float fireCountdown = 0f;

    public Transform firePoint;

    public bool canRotate;

    void Start() {
        InvokeRepeating("UpdateTarget", 0f, 0.25f);

    }

    // Update is called once per frame
    void UpdateTarget() {
        //coge en un array todos los GameObjects con una tag "enemyTag"
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        //calcular enemigo más cercano (nearestEnemy)
        //a cada enemigo calcula su distancia y si ese tiene menos que todos los demás lo añade a nearestEnemy
        foreach (GameObject enemy in enemies) {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance) {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        //Asignar el enemigo más cercano a su variable
        //si no hay un nearestEnemy y hay un enemigo cercano a rango apunta hacia ese enemigo
        if (nearestEnemy != null && shortestDistance <= range) {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
            if (canRotate){
                if (target.position.x < transform.position.x) {
                    this.transform.rotation = new Quaternion(this.transform.rotation.x, 180, this.transform.rotation.z, 0);
                } else {
                    this.transform.rotation = new Quaternion(this.transform.rotation.x, 0, this.transform.rotation.z, 0);
                }
            }
        } else {
            target = null;
        }
    }

    void Update() {
        if (target == null)
            return;
        //cada frame mira si puede hacer LockOnTarget y si ha disparado hace el cooldown, tambien si ha disparado para la animacion de disparo
        //LockOnTarget();
        if (fireCountdown <= 0f) {
            Shoot();
            fireCountdown = 1f / fireRate;
        } else {
            //gameObject.GetComponent<Animator>().SetBool("Shoot", false);
            fireCountdown -= Time.deltaTime;
        }
    }
    //Disparar al objetivo
    void Shoot() {
        //crear la bala y ponerle su objetivo, si este está a la izquierda se le dice
        GameObject bulletGO = (GameObject)Instantiate(bulletPref, firePoint.position, new Quaternion(0, 0, 0, 0));
        bullet_homing bullet = bulletGO.GetComponent<bullet_homing>();
        if (target.transform.position.x < transform.position.x) {
            bullet.left = true;
        }
        //le envia quien es el objetivo a la bala y la torreta hace la animacion de disparar
        if (bullet != null) {
            bullet.Seek(target);
            //gameObject.GetComponent<Animator>().SetBool("Shoot", true);
        }
    }

    //crear visualmente el rango
    void OnDrawGizmos() {

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
