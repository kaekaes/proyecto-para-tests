﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [Header("Speed")]
    public float startspeed = 1f;
    public float speed;

    [Header("Health")]
    public int starthealth;
    public int health;

    void Start() {
        speed = startspeed;
        health = starthealth;
    }
}
