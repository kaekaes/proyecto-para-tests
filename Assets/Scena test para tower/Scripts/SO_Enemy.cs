﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New enemy", menuName = "Enemy")]
public class SO_Enemy : ScriptableObject {

    public string enemyname;
    public float mov_speed;
    public int health;

    public Sprite Icon;
}
