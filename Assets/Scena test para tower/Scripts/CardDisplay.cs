﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardDisplay : MonoBehaviour {

    public SO_Card card;

    public Sprite[] rarities = new Sprite[21];
    public Image card_rarity;
    public Image card_bg;
    public Text cardname;
    public Image icon;
    public Text dmg;
    public Text range;
    public Text att_speed;
    public Text special;
    public Outline special_color;
    public Text price;


    void Start() {
        selectRarity();
        card_rarity.sprite = card_rarity.sprite;
        card_bg.sprite = card.card_bg;
        cardname.text = card.cardname.ToString();
        icon.sprite = card.Icon;
        dmg.text = card.dmg.ToString();
        range.text = card.range.ToString();
        att_speed.text = card.att_speed.ToString();
        special.text = card.special.ToString();
        special_color.effectColor = card.special_color;
        price.text = card.price.ToString();
    }
    public void selectRarity() {
        if ((card.card_commonness == 0)&&(card.card_rarity == 0)) {
            card_rarity.sprite = rarities[0];
        }
        else if ((card.card_commonness == 0) && (card.card_rarity == 1)) {
            card_rarity.sprite = rarities[1];
        }
        else if ((card.card_commonness == 0) && (card.card_rarity == 2)) {
            card_rarity.sprite = rarities[2];
        } 
        else if ((card.card_commonness == 0) && (card.card_rarity == 3)) {
            card_rarity.sprite = rarities[3];
        } 
        else if ((card.card_commonness == 1) && (card.card_rarity == 0)) {
            card_rarity.sprite = rarities[4];
        } 
        else if ((card.card_commonness == 1) && (card.card_rarity == 1)) {
            card_rarity.sprite = rarities[5];
        } 
        else if ((card.card_commonness == 1) && (card.card_rarity == 2)) {
            card_rarity.sprite = rarities[6];
        } 
        else if ((card.card_commonness == 1) && (card.card_rarity == 3)) {
            card_rarity.sprite = rarities[7];
        } 
        else if ((card.card_commonness == 2) && (card.card_rarity == 0)) {
            card_rarity.sprite = rarities[8];
        } 
        else if ((card.card_commonness == 2) && (card.card_rarity == 1)) {
            card_rarity.sprite = rarities[9];
        } 
        else if ((card.card_commonness == 2) && (card.card_rarity == 2)) {
            card_rarity.sprite = rarities[10];
        } 
        else if ((card.card_commonness == 2) && (card.card_rarity == 3)) {
            card_rarity.sprite = rarities[11];
        } 
        else if ((card.card_commonness == 3) && (card.card_rarity == 0)) {
            card_rarity.sprite = rarities[12];
        } 
        else if ((card.card_commonness == 3) && (card.card_rarity == 1)) {
            card_rarity.sprite = rarities[13];
        } 
        else if ((card.card_commonness == 3) && (card.card_rarity == 2)) {
            card_rarity.sprite = rarities[14];
        } 
        else if ((card.card_commonness == 3) && (card.card_rarity == 3)) {
            card_rarity.sprite = rarities[15];
        } 
        else if ((card.card_commonness == 4) && (card.card_rarity == 0)) {
            card_rarity.sprite = rarities[16];
        } 
        else if ((card.card_commonness == 4) && (card.card_rarity == 1)) {
            card_rarity.sprite = rarities[17];
        } 
        else if ((card.card_commonness == 4) && (card.card_rarity == 2)) {
            card_rarity.sprite = rarities[18];
        } 
        else if ((card.card_commonness == 4) && (card.card_rarity == 3)) {
            card_rarity.sprite = rarities[19];
        } 
        else if ((card.card_commonness == 5) && (card.card_rarity == 0)) {
            card_rarity.sprite = rarities[20];
        }
    }
}
