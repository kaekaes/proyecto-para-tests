﻿using UnityEngine;

public class bullet : MonoBehaviour {

    [Header("General")]
    private Transform target;
    public float speed = 40f;
    public int damage = 4;

    [Header("Efectos")]
    public GameObject impactEffect;
    public bool doImpactEffect = false;

    //Coger desde el script tower el target
    public void Seek(Transform _target) {
        target = _target;
    }

    void Update() {

        //Si no tiene objetivo desaparece
        if (target == null) {
            Destroy(gameObject);
            return;
        }

        //Calcular posicion y avanzar hacia target
        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame) {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        //transform.LookAt(target);
    }

    void HitTarget() {
        //Falta añadir que dañe el objetivo
        if (doImpactEffect) {
            GameObject effectInstance = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
            Destroy(effectInstance, 1f);
        } else {
            impactEffect = null;
        }
        Destroy(gameObject);

        //por ahora destruye al enemigo directamente
        //Destroy(target.gameObject);

        //EJEMPLO
        //PlayerStats.Money += 30;
    }
}
