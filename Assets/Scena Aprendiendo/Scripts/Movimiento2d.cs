﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento2d : MonoBehaviour {

    public Rigidbody2D rb;
    public float movSpeed = 5f;
    public float rotSpeed = 5f;
    public Vector2 mov;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	void Update () {


        /*mov = new Vector2(
            Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical")
        );

        transform.position = Vector3.MoveTowards(
            transform.position,
            transform.position + mov,
            movSpeed * Time.deltaTime


        );*/

        //Movimiento en 4 ejes cordinales
        //Este primero funciona, pero no mientras cambies la orientacion
        /*if (Input.GetKey(KeyCode.W)) {
            transform.position = new Vector3(transform.position.x, transform.position.y + movSpeed * Time.deltaTime, transform.position.z);
        }
        if (Input.GetKey(KeyCode.S)) {
            transform.position = new Vector3(transform.position.x, transform.position.y - movSpeed * Time.deltaTime, transform.position.z);
        }
        if (Input.GetKey(KeyCode.D)) {
            transform.position = new Vector3(transform.position.x + movSpeed * Time.deltaTime, transform.position.y, transform.position.z);
        }
        if (Input.GetKey(KeyCode.A)) {
            transform.position = new Vector3(transform.position.x - movSpeed * Time.deltaTime, transform.position.y, transform.position.z);
        }

        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z + rotSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z - rotSpeed * Time.deltaTime, 0);
        }*/

        if (Input.GetKey(KeyCode.W)) {
            transform.Translate(0, movSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.S)) {
            transform.Translate(0, -movSpeed * Time.deltaTime, 0);
        }

        if (Input.GetKey(KeyCode.E)) {
            transform.Translate(movSpeed * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(KeyCode.Q)) {
            transform.Translate(-movSpeed * Time.deltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.D)) {
            transform.Rotate(0, 0, -rotSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A)) {
            transform.Rotate(0, 0, rotSpeed * Time.deltaTime);
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        if(col.gameObject.tag != "suelo"){
            Destroy(col.gameObject);
            Debug.Log(col);
        } else {
            return;
        }
        
    }
}
