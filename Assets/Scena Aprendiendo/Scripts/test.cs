﻿using UnityEngine;

public class test : MonoBehaviour {

    public bool movimientoN = false;
    public float x = 5;
    public float y = 5.1f;
    public float z = 5;

	void Start () {

    }

    void Update () {
        if (Input.GetKey(KeyCode.W)) {
            transform.Translate(new Vector3(0, 0, z));
        }
        if (Input.GetKey(KeyCode.S)) {
            transform.Translate(new Vector3(0, 0, -z));
        }
        if (Input.GetKey(KeyCode.A)) {
            transform.Rotate(Vector3.down);
        }
        if (Input.GetKey(KeyCode.D)) {
            transform.Rotate(Vector3.up);
        }
    }
}
